##Advanced Browser##

This add-on is licensed under GPLv3.


The core component of this add-on lets you easily add and manage custom columns in the card browser (from other add-ons). Advanced Browser comes bundled with another component that makes use of the core add-on to provide columns for any field in your notes.

Other useful fields include:
- First review date
- Latest review date
- Average answer time
- Total answer time
- Tags

This is a work-in-progress and not yet available on AnkiWeb.